from django.shortcuts import render
from apps.tasks.models import Task
from apps.tasks.serializers import TaskSerializer
from apps.projects.serializers import ProjectSerializer,ProjectPostSerializer
from apps.comments.models import Comment
from apps.projects.models import Project
from rest_framework.response import Response
from rest_framework.views import APIView

class Projects(APIView):
    serializer_class = ProjectSerializer
    serializer_class_post = ProjectPostSerializer
    def get(self,request,project=None,format=None):
        if project != None:
            project = Project.objects.get(id=project)
            serializer = self.serializer_class(project,many=False)
            return Response(serializer.data)
        else:
            projects = Project.objects.all()
            if len(projects) > 0:
                if len(projects) == 1:
                    projects = projects.first()
                    many = False
                elif len(projects) > 1:
                    many = True

                serializer = self.serializer_class(projects,many=many)
                if(many):
                    for sproject in serializer.data:
                        sproject['total_tasks'] = Task.objects.filter(project=sproject['id']).count()

                    return Response({"data":serializer.data,"many":many})
                else:
                    sproject = serializer.data
                    sproject['total_tasks'] = Task.objects.filter(project=sproject['id']).count()

                    return Response({"data":serializer.data,"many":many})
            else:
                return Response({"data":None,"many":False})
    def post(self,request,project=None,format=None):
        if project != None:
            project = Project.objects.get(id=project)
            serializer = self.serializer_class_post(project,data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                return Response({"message":"403 Forbidden","errors": serializer.errors})
        else:
            serializer = self.serializer_class_post(data=request.data)
            if serializer.is_valid():
                serializer.save()

                new_project = Project.objects.get(id=serializer.data['id'])
                serializer_project = self.serializer_class(new_project,many=False)
                return Response(serializer_project.data)
            else:
                return Response({"message":"403 Forbidden","errors":serializer.errors})

    def delete(self,request,project,format=None):
        project_remove = Project.objects.get(id=project)
        if project_remove.delete():
            return Response({"message":"delete","id":project})
        else:
            return Response({"message":"403 Forbidden"})

class ProjectTasks(APIView):
    serializer_class = TaskSerializer

    def get(self,request,project,format=None):
        tasks = Task.objects.filter(project=project)
        if len(tasks) > 0:
            if len(tasks) == 1:
                tasks = tasks.first()
                many = False
            elif len(tasks) > 1:
                many = True

            serializer = self.serializer_class(tasks,many=many)

            if many:
                for stask in serializer.data:
                    local_task = Task.objects.get(id=stask['id'])
                    stask["total_developers"] = local_task.developers().count()
                    stask["total_subtasks"] = local_task.subtasks().count()
                    stask["total_comments"] = Comment.objects.filter(task=stask['id']).count()
                    stask["is_developer"] = local_task.is_developer(request.user.id)
                return Response({ "data": serializer.data, "many": many})
            else:
                stask = serializer.data
                local_task = Task.objects.get(id=stask['id'])
                stask["total_developers"] = local_task.developers().count()
                stask["total_subtasks"] = local_task.subtasks().count()
                stask["total_comments"] = Comment.objects.filter(task=stask['id']).count()
                stask["is_developer"] = local_task.is_developer(request.user.id)
                return Response({ "data": stask, "many": many})
        else:
            return Response({ "data": None,"many":False})

    def post(self,request,project,format=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response({"message":"403 Forbidden"})

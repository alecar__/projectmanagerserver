from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from apps.projects.views import *

urlpatterns = [
    url(r'^$', Projects.as_view()),
    url(r'^(?P<project>\d+)/$', Projects.as_view()),
    url(r'^(?P<project>\d+)/tasks/$', ProjectTasks.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)

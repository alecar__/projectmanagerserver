from django.contrib.auth.models import User
from django.db import models

class Project(models.Model):
    name = models.CharField(max_length=120, null=False,blank=False)
    description = models.TextField(max_length=300, null=False,blank=False)
    avatar = models.ImageField(upload_to='images_projects')
    alias = models.CharField(max_length=120, null=False,blank=False,unique=True)
    status = models.BooleanField(default=False)
    initial_date = models.DateField()
    final_date = models.DateField()
    leader_user = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.alias)

    def change_status(self):
        self.status = not self.status
        self.save()

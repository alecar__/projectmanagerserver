from rest_framework import serializers
from apps.projects.models import Project
from apps.users.serializers import UserSerializer

class ProjectSerializer(serializers.ModelSerializer):
    leader_user = UserSerializer()
    class Meta:
        model = Project
        fields = "__all__"

class ProjectPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = "__all__"

from django.conf.urls import url
from apps.comments.views import *
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    #url(r'^getcomments/$', getComments.as_view()),
    url(r'^$', Comments.as_view()),
    url(r'^(?P<comment>\d+)/$', Comments.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)

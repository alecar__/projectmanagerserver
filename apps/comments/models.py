from django.contrib.auth.models import User
from django.db import models
from apps.tasks.models import Task

class Comment(models.Model):
    task = models.ForeignKey(Task,on_delete=models.CASCADE)
    parent_comment = models.IntegerField(null=True,blank=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    title = models.CharField(max_length=120,null=False,blank=False)
    comment = models.TextField(max_length=300,null=False,blank=False)

    def __str__(self):
        return '{}'.format(self.title)

    def subcomments(self):
        return Comment.objects.filter(parent_comment=self.id)

from django.shortcuts import render
from apps.comments.models import Comment
from apps.comments.serializers import CommentGetSerializer,CommentPostSerializer
from rest_framework.response import Response
from rest_framework.views import APIView

class Comments(APIView):
    serializer_class_get = CommentGetSerializer
    serializer_class_post = CommentPostSerializer
    def get(self,request,comment=None,format=None):
        if comment != None:
            comment = Comment.objects.get(id=comment)
            serializer = self.serializer_class_get(comment,many=False)
            return Response(serializer.data)
        else:
            comments = Comment.objects.all()
            if len(comments) > 0:
                if len(comments) == 1:
                    many = False
                elif len(comments) > 1:
                    many = True

                serializer = self.serializer_class_get(comments,many=many)
                return Response(serializer.data)
            else:
                return Response([])

    def post(self,request,comment=None,format=None):
        if comment != None:
            comment = Comment.objects.get(id=comment)
            serializer = self.serializer_class_post(comment,data=request.data,partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                return Response({"message":"403 Forbidden","errors": serializer.errors})
        else:
            serializer = self.serializer_class_post(data=request.data)
            if serializer.is_valid():
                serializer.save()
            
                return Response(serializer.data)
            else:
                return Response({"message":"403 Forbidden","errors": serializer.errors})

from rest_framework import serializers
from apps.comments.models import Comment
from apps.users.serializers import UserSerializer
from apps.tasks.serializers import TaskSerializer

class CommentGetSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    task = TaskSerializer()
    class Meta:
        model = Comment
        fields = "__all__"

class CommentPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = "__all__"

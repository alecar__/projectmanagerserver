from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from apps.users.serializers import UserSerializer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from django.contrib.auth.decorators import login_required

@login_required
def Home(request):
    return render(request,'home/index.html',context={})

class UserViewSet(ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

def current_user(request):
    user = User.objects.get(id=request.user.id)
    serialize_class = UserSerializer
    suser = serialize_class(user,many=False)
    return Response(suser.data)

class CurrentUser(APIView):
    serializer_class = UserSerializer

    def get(self,request,format=None):
        user = User.objects.get(id=request.user.id)
        serializer = self.serializer_class(user,many=False)
        return Response(serializer.data)

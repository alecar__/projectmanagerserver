from django.conf.urls import url,include
from rest_framework.urlpatterns import format_suffix_patterns
from apps.users.views import *
from rest_framework.routers import SimpleRouter

router = SimpleRouter()
router.register("users",UserViewSet)
urlpatterns = router.urls

#urlpatterns = format_suffix_patterns(urlpatterns)

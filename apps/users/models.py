from django.contrib.auth.models import User
from django.db import models
# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    avatar = models.ImageField(upload_to='images_profile')

    def __str__(self):
        return '{}'.format(self.user.username)

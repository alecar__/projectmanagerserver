from django.shortcuts import render
from apps.tasks.models import Task,TaskUser
from apps.tasks.serializers import TaskSerializer,TaskUserGetSerializer,TaskUserPostSerializer
from apps.comments.models import Comment
from apps.comments.serializers import CommentGetSerializer
from rest_framework.response import Response
from rest_framework.views import APIView


class Tasks(APIView):
    serializer_class = TaskSerializer

    def get(self,request,task=None,format=None):
        if task != None:
            task = Task.objects.get(id=task)
            serializer = self.serializer_class(task,many=False)
            return Response(serializer.data)
        else:
            tasks = Task.objects.all()
            if len(tasks) > 0:
                if len(tasks) == 1:
                    many = False
                elif len(tasks) > 1:
                    many = True

                serializer = self.serializer_class(tasks,many=many)

                if many:
                    for stask in serializer.data:
                        developer = TaskUser.objects.filter(task=stask['id'],user=request.user.id).count()
                        if developer > 0:
                            stask['is_developer'] = True
                        else:
                            stask['is_developer'] = False

                    return Response({"data":serializer.data,"many":many})
                else:
                    stask = serializer.data
                    developer = TaskUser.objects.filter(task=stask['id'],user=request.user.id).count()
                    if developer > 0:
                        stask['is_developer'] = True
                    else:
                        stask['is_developer'] = False

                    return Response({"data":stask,"many":many})
            else:
                return Response({"data":None,"many":False})
    def post(self,request,task=None,format=None):
        if task != None:
            task = Task.objects.get(id=task)
            serializer = self.serializer_class(task,data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                return Response({"message":"403 Forbidden","errors": serializer.errors})
        else:
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                return Response({"message":"403 Forbidden"})

    def patch(self,request,task,format=None):
        local_task = Task.objects.get(id=task)
        serializer = self.serializer_class(local_task,data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response({"message":"403 Forbidden","errors": serializer.errors})

    def delete(self,request,task,format=None):
        task_remove = Task.objects.get(id=task)
        if task_remove.delete():
            return Response({"message":"delete","id":task})
        else:
            return Response({"message":"403 Forbidden"})

class TaskComments(APIView):
    serializer_class = CommentGetSerializer

    def get(self,request,task,format=None):
        comments = Comment.objects.filter(task=task)
        if len(comments) > 0:
            if len(comments) == 1:
                comments = comments.first()
                many = False
            elif len(comments) > 1:
                many = True

            serializer = self.serializer_class(comments,many=many)
            return Response({"data":serializer.data,"many":many})
        else:
            return Response([])

class TaskUsers(APIView):
    serializer_class_get = TaskUserGetSerializer
    serializer_class_post = TaskUserPostSerializer

    def get(self,request,task,user=None,format=None):
        if user != None:
            developer = TaskUser.objects.get(task=task,user=user)
            serializer = self.serializer_class_get(developer,many=False)
            return Response(serializer.data)
        else:
            task = Task.objects.get(id=task)
            developers = task.developers()
            if len(developers) > 0:
                if len(developers) == 1:
                    developers = developers.first()
                    many = False
                elif len(developers) > 1:
                    many = True

                serializer = self.serializer_class_get(developers,many=many)
                return Response({"data":serializer.data,"many":many})
            else:
                return Response({})
    def post(self,request,task,user=None,format=None):
        if user != None:
            task = TaskUser.objects.get(task=task,user=user)
            serializer = self.serializer_class_post(task,data=request.data)
            if serializer.is_valid():
                serializer.save()
                task.task.calculate()
                return Response(serializer.data)
            else:
                return Response({"message":"403 Forbidden","errors": serializer.errors})
        else:
            taskuser = TaskUser.objects.filter(user=request.data["user"],task=request.data["task"])
            if len(taskuser) > 0:
                return Response({"message":"403 Forbidden"})
            else:
                serializer = self.serializer_class_post(data=request.data)
                if serializer.is_valid():
                    serializer.save()
                    newdeveloper = TaskUser.objects.get(id=serializer.data['id'])
                    new_serialize = self.serializer_class_get(newdeveloper,many=False)
                    return Response(new_serialize.data)
                else:
                    return Response({"message":"403 Forbidden"})
    def patch(self,request,task,user,format=None):
        local_taskuser = TaskUser.objects.get(task=task,user=user)
        serializer = self.serializer_class_get(local_taskuser,data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response({"message":"403 Forbidden","errors": serializer.errors})

class TaskSubtasks(APIView):
    serializer_class = TaskSerializer
    def get(self,request,task,format=None):
        tasks = Task.objects.get(id=task)
        subtasks = tasks.subtasks()
        if len(subtasks):
            if len(subtasks) == 1:
                subtasks = subtasks.first()
                many = False
            elif len(subtasks) > 1:
                many = True

            serializer = self.serializer_class(subtasks,many=many)
            return Response([serializer.data])
        else:
            return Response([])

tasks = Tasks.as_view()
taskusers = TaskUsers.as_view()
taskcomments = TaskComments.as_view()
subtasks = TaskSubtasks.as_view()

from django.contrib.auth.models import User
from django.db import models
from apps.projects.models import Project

class Task(models.Model):
    STATUS = (
        (0, 'ToDo'),
        (1, 'InProcess'),
        (2, 'Finished')
    )

    project = models.ForeignKey(Project,on_delete=models.CASCADE)
    name = models.CharField(max_length=120,null=False,blank=False)
    description = models.TextField(max_length=300,null=False,blank=False)
    alias = models.CharField(max_length=120, null=False,blank=False,unique=True)
    status = models.IntegerField(max_length=1, choices=STATUS, default=0)
    initial_date = models.DateField()
    final_date = models.DateField()
    total_time = models.IntegerField()
    percentage = models.FloatField()
    parent_task = models.IntegerField(null=True,blank=True)


    def __str__(self):
        return '{}'.format(self.alias)

    def to_assign(self,users):
        for user in users:
            developer = User.objects.get(id=user)
            new_task_user = TaskUser(user=developer,task=self,total_time=0,percentage=0)
            new_task_user.save()

        return self.developers()

    def developers(self):
        return TaskUser.objects.filter(task=self)

    def subtasks(self):
        return Task.objects.filter(parent_task=self.id)

    def is_developer(self,user):
        developer = TaskUser.objects.filter(task=self,user=user)
        if len(developer) > 0:
            return True
        else:
            return False

    def calculate(self):
        developers = self.developers()
        if len(developers) > 0:
            percentage = 0
            total_time = 0
            for developer in developers:
                percentage = percentage+developer.percentage
                total_time = total_time+developer.total_time

            self.total_time = total_time
            self.percentage = percentage / len(developers)
            self.save()

            return {'error': False, 'data': {'total_time': self.total_time, 'percentage': self.percentage}}
        else:
            return {'error': True, 'message': "Esta tarea no tiene usuarios asignados"}

class TaskUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    total_time = models.IntegerField()
    percentage = models.FloatField()

    def __str__(self):
        return 'user: {} - task: {}'.format(self.user.username,self.task.alias)

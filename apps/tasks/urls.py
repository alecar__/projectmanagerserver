from django.conf.urls import url,include
from apps.tasks.views import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^$', Tasks.as_view()),
    url(r'^(?P<task>\d+)/$', Tasks.as_view()),
    url(r'^(?P<task>\d+)/comments/$',TaskComments.as_view()),
    url(r'^(?P<task>\d+)/developers/$',TaskUsers.as_view()),
    url(r'^(?P<task>\d+)/developers/(?P<user>\d+)/$',TaskUsers.as_view()),
    url(r'^(?P<task>\d+)/subtasks/$',TaskSubtasks.as_view()),
]

from rest_framework import serializers
from apps.tasks.models import Task,TaskUser
from apps.users.serializers import UserSerializer

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = "__all__"

class TaskUserGetSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    task = TaskSerializer()
    class Meta:
        model = TaskUser
        fields = "__all__"

class TaskUserPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskUser
        fields = "__all__"
